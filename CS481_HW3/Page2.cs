﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CS481_HW3
{
    public class Page2 : ContentPage
    {
        public Page2()
        {
            var EndButton = new Button { Text = "24", VerticalOptions = LayoutOptions.CenterAndExpand };
            EndButton.Clicked += GoToRoot;

            // I wanted to input an image of Kobe wearing 24 but ran out of time
            Content = new StackLayout
            {
                Children = {
                    EndButton
                }
            };
        }
        async void GoToRoot(object sender, System.EventArgs e)
        {
            await NavigationEventArgs.PopToRootAsync();  //Returning to the starting page
        }
    }
}