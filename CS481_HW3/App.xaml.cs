﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//Adapted From: https://github.com/xamarin/xamarin-forms-samples/tree/master/Navigation/Hierarchical/WorkingWithNavigation

namespace CS481_HW3
{
    public partial class App : Application
    {
        public App()
        {
            MainPage = new NavigationPage(new Page1()); //creating first page
            
            var MoveForwardButton = new Button { Text = "Click qwik", VerticalOptions = LayoutOptions.CenterAndExpand };
            MoveForwardButton += Handle_Appearing;

           
        }

        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {

        }

        protected override void OnResume()
        {

        }

        async void Handle_Appearing(object sender, System.EventArgs e)
        {
            await NavigationEventArgs.PushAsync(new Page1());//Called when first page appearing
        }

       
        // hol up
        async void Handle_Disappearing(object sender, System.EventArgs e)
        {
            await NavigationEventArgs.PopAsync();  //Called when leaving page
        }

        async void GoToRoot(object sender, System.EventArgs e)
        {
            await NavigationEventArgs.PopToRootAsync();  //Returning to the starting page
        }
    }
}
