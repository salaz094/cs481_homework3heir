﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CS481_HW3
{
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            var AddingButton = new Button { Text = "23 + 1", VerticalOptions = LayoutOptions.CenterAndExpand };
            AddingButton += AddClicked;

            var MinusButton = new Button { Text = "12-4", VerticalOptions = LayoutOptions.CenterAndExpand };
            MinusButton += SubClicked;

            Content = new StackLayout
            {
                Children = {
                   AddingButton, MinusButton
                }
            };

             async void AddClicked(object sender, EventArgs e)
            {
                await Navigation.PushAsync(new Page2());
            }

            async void SubClicked(object sender, EventArgs e)
            {
                await Navigation.PushAsync(new Page3());
            }
        }
       
        
    }
}