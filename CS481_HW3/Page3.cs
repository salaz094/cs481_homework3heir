﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace CS481_HW3
{
    public class Page3 : ContentPage
    {
        public Page3()
        {
            var EndButton = new Button { Text = "8", VerticalOptions = LayoutOptions.CenterAndExpand };
            EndButton.Clicked += GoToRoot;
            //Wanted a picture of Frobe wearing 8 but lacked time.
            Content = new StackLayout
            {
                Children = {
                    EndofAppButton
                    
                }
            };

            async void GameOverClicked(object sender, EventArgs e)
            {
                await DisplayAlert("Hey-Hey-Hey", "Game Over bro, Game Over", "OK");

            }

            
        }
    }
}