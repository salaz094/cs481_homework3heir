﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        async void GameOverClicked(object sender, EventArgs e)
        {
            await DisplayAlert("Hey-Hey-Hey", "Game Over bro, Game Over", "OK");
        }

       
    }
}